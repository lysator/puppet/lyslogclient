class lyslogclient
{
  file {
    '/etc/rsyslog.d/lysator.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => 'puppet:///modules/lyslogclient/lysator.conf';
  }

  ensure_packages(['rsyslog'],
    { ensure => 'installed' })

  service {
    'rsyslog':
      ensure    => 'running',
      enable    => true,
      pattern   => 'syslog',
      subscribe => File['/etc/rsyslog.d/lysator.conf'];
  }

  cron {
    'syslog_mark':
      ensure  => present,
      command => '/usr/bin/logger mark',
      user    => root,
      minute  => 43,
  }
}
